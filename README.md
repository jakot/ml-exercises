These are solutions to some exercises covering various machine learning topics, made during studying the field, aimed at showing off knowledge of ML and set of familiar tools. More projects are to be made and included here or in another repo.

- most relevant (most to least on the list):
    - [flowers_gan.ipynb](flowers_gan.ipynb) - implementation of FastGAN in TensorFlow trained on a dataset of flower photos collected from a website manually,
    - [pruning.ipynb](pruning.ipynb) - pruning algorithms (namely edge-popup and iterative randomization) implementations and their performance comparison on MNIST dataset,
    - [cnn_from_scratch.ipynb](cnn_from_scratch.ipynb) - a cnn implementation made from scratch,

- classic ml techniques (no particular order):
    - [spam_clf.ipynb](spam_clf.ipynb) - simple spam classifier with a little data preparation and quick training,
    - [simple_blender.ipynb](simple_blender.ipynb) - voting classifier working with mnist,
    - [random_trees.ipynb](random_trees.ipynb) - short play with random trees and moons,
    - [olivetti.ipynb](olivetti.ipynb) - trying KMeans, PCA and gaussian mixtures with olivetti dataset,
    - [knn_mnist.ipynb](knn_mnist.ipynb) - trying to reach as high as possible accuracy using KNN on mnist,
    - [decomposition.ipynb](decomposition.ipynb) - PCA and few others decomposition algorithms used on mnist,
- artificial neural networks (no particular order):
    - [variational_photos.ipynb](variational_photos.ipynb) - trying some variational autoencoder on two image datasets,
    - [transfer_cnn.ipynb](transfer_cnn.ipynb) - transfer learning applied for building a cnn dealing with specific set of photos,
    - [sketch_rnn.ipynb](sketch_rnn.ipynb) - an rnn dealing with SketchRNN dataset decoded from protobuffs,
    - [shakespeare_transformers.ipynb](shakespeare_transformers.ipynb) - a pretrained GPT2 model used to generate shakespearean text,
    - [reinforced_lander.ipynb](reinforced_lander.ipynb) - policy gradients applied to a gym's lander,
    - [reber_rnn.ipynb](reber_rnn.ipynb) - small GRU model to deal with Reber Grammar,
    - [movies_ds.ipynb](movies_ds.ipynb) - movie review dataset processed by a model comprising embedding layer,
    - [fast_mnist.ipynb](fast_mnist.ipynb) - a small quick nn working on mnist,
    - [fashion_mnist_ds.ipynb](fashion_mnist_ds.ipynb) - using tf.datasets to prepare fashion mnist and feed some nn with it,
    - [date_converter.ipynb](date_converter.ipynb) - an encoder-decoder model converting date from one format to another,
    - [cifar10.ipynb](cifar10.ipynb) - trying few simple nns on cifar10,
    - [cifar10_autoencoder.ipynb](cifar10_autoencoder.ipynb) - building autoencoder on cifar10 and using its layers in classifier,
    - [chorales.ipynb](chorales.ipynb) - some RNN trained on JSB chorales and struggling to generate new music,
    - [ann_mnist.ipynb](ann_mnist.ipynb) - a nn fed with mnist (not that handy as the previous one).